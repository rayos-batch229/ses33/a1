// console.log("Hello World!")

// fetch("https://jsonplaceholder.typicode.com/todos")

// .then((response) => response.json())
// .then((json) => console.log(json));

// console.log(fetch("https://jsonplaceholder.typicode.com/todos")
// 	.then(response => console.log(response.status)));
fetch("https://jsonplaceholder.typicode.com/todos")
.then(response => response.json())
.then((json) => json.map(({title}) => title))
.then((titles) => console.log(titles))


fetch("https://jsonplaceholder.typicode.com/todos/1")
.then((response) => response.json())
.then((json) => console.log(json));

// POST
fetch("https://jsonplaceholder.typicode.com/todos", 
{
	method: "POST",
	headers:  {
		"Content-Type" : "application/json"
	},
	body: JSON.stringify({
		userId: "201",
		id: "201",
		title: "New Post",
		completed: true
	})
})
.then((response) => response.json())
.then((json) => console.log(json));

// PUT
fetch("https://jsonplaceholder.typicode.com/todos/1", 
{
	method: "PUT",
	headers:  {
		"Content-Type" : "application/json"
	},
	body: JSON.stringify({
		"Title":"Update Post!",
		"Description": "delectus aut autem",
		"Status": "Completed",
		"Date Completed": "01/16/2023",
		"User ID": "1"
	})
})
.then((response) => response.json())
.then((json) => console.log(json));

// PATCH
fetch("https://jsonplaceholder.typicode.com/todos/1", 
{
	method: "PATCH",
	headers:  {
		"Content-Type" : "application/json"
	},
	body: JSON.stringify({
		Title: "Corrected Post using PATCH"
	})
})
.then((response) => response.json())
.then((json) => console.log(json));

// DELETE
fetch("https://jsonplaceholder.typicode.com/todos/1", {
	method: "DELETE"
});




